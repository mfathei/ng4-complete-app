import { Component, OnDestroy } from '@angular/core';
import { DataStoreService } from '../../shared/data-store.service';
import { Subscription } from 'rxjs/Subscription';
import { Response } from '@angular/http';
import { AuthService } from '../../auth/auth.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent implements OnDestroy {
    private subscription: Subscription;

    constructor(private dataStoreService: DataStoreService, public authService: AuthService) { }

    onSave() {
        this.subscription = this.dataStoreService.saveReipes().subscribe(
            (res: Response) => { console.log(res); },
            (err: Response) => { console.log(err); }
        );
    }

    onFetchData() {
        this.dataStoreService.getRecipes();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    onLogout() {
        this.authService.logout();
    }
}