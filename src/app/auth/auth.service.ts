import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
    private token: string = null;

    constructor(private router: Router) { }

    signupUser(email: string, password: string) {
        console.log('signupUser');
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((res) => {
                this.router.navigate(['/signin']);
            })
            .catch(error => console.log(error));
    }

    signinUser(email: string, password: string) {
        console.log('signinUser');
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(res => {
                this.router.navigate(['/']);
                firebase.auth().currentUser.getIdToken().then(
                    (token: string) => {
                        this.token = token;
                    }
                );
            })
            .catch(err => console.log(err));
    }

    getToken() {
        firebase.auth().currentUser.getIdToken().then(
            (token: string) => {
                this.token = token;
            }
        );
        return this.token;
    }

    isAuthenticated(): boolean {
        return this.token !== null;
    }

    logout() {
        firebase.auth().signOut();
        this.token = null;
        this.router.navigate(['/signin']);
    }

}