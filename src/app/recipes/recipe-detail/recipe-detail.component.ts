import { Component, OnInit, Input } from '@angular/core';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  recipe: Recipe;
  id: number;

  constructor(private routes: ActivatedRoute, private recipeService: RecipeService,
    private router: Router, private authService: AuthService) { }

  ngOnInit() {
    this.routes.params.subscribe((params: Params) => {
      this.id = +params['id'];
      this.recipe = this.recipeService.getRecipe(this.id);
    });
  }

  addToShoppingList() {
    this.recipeService.addToShoppingList(this.recipe.ingredients);
  }

  onEditRecipe() {
    this.router.navigate(['edit'], { relativeTo: this.routes });
  }

  onDeleteRecipe() {
    if (!this.authService.isAuthenticated()) {
      alert("Please signin first");
      return false;
    }
    this.recipeService.deleteRecipe(this.id);
    this.router.navigate(['/recipes']);
  }
}
