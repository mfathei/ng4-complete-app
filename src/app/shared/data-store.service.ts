import { Injectable } from "@angular/core";
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';

import { Recipe } from "../recipes/recipe.model";
import { RecipeService } from "../recipes/recipe.service";
import { AuthService } from "../auth/auth.service";

export const URL = 'https://ng-recipe-app-f6d8c.firebaseio.com/recipes.json?auth=';

@Injectable()
export class DataStoreService {

    constructor(private http: Http, private recipeService: RecipeService, private authService: AuthService) { }

    saveReipes() {
        let token = this.authService.getToken();
        return this.http.put(URL + token, this.recipeService.getRecipes());
    }

    getRecipes() {
        let token = this.authService.getToken();
        this.http.get(URL + token).map(
            (res: Response) => {
                const recipes: Recipe[] = res.json();
                for (let recipe of recipes) {
                    if (!recipe['ingredients']) {
                        recipe.ingredients = [];
                    }
                }
                console.log(recipes);
                return recipes;
            }
        ).subscribe((recipes: Recipe[]) => {
            this.recipeService.setRecipes(recipes);
        });
    }

}